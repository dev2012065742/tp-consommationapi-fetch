// import React, { useEffect, useState } from "react";
// import User from "./User";
// import "./App.css";

// export default function App() {
//   const [users, setUsers] = useState([]);

//   useEffect(() => {
//     const gettingUsers = async () => {
//       const response = await fetch("https://jsonplaceholder.typicode.com/users")
//         .then((e) => e.json())
//         .then((res) => {
//           setUsers(res);
//           console.log("Users :", users);
//         });
//     };

//     gettingUsers();
//   }, []);
//   return (
//     <div className="container">
//       <h1>nombre d'utilisateurs: {users.length}</h1>
//       <div className="sub-container">
//         {users.map((user) => (
//           <User user={user} />
//         ))}
//       </div>
//     </div>
//   );
// }

// *****************************************************************************

import React, { Component } from "react";
import User from "./User";
import "./App.css";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
    };
  }
  componentDidMount() {
    const gettingUsers = async () => {
      const response = await fetch("https://jsonplaceholder.typicode.com/users")
        .then((e) => e.json())
        .then((res) => {
          console.log("res users :", res);
          this.setState({ users: res });
          console.log("Users :", this.state.users);
        });
    };
    gettingUsers();
  }

  render() {
    return (
      <div className="container">
        <h1>nombre d'utilisateurs: {this.state.users.length}</h1>
        <div className="sub-container">
          {this.state.users.map((user, index) => (
            <User user={user} index={index} />
          ))}
        </div>
      </div>
    );
  }
}
