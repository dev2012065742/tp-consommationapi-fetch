// import React from "react";

// export default function Post({ post }) {
//   console.log(post);
//   return (
//     <div className="post">
//       <h4>{post.title}</h4>
//       <p>{post.body}</p>
//     </div>
//   );
// }

// **************************************************

import React, { Component } from "react";

export default class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: props.post,
    };
    console.log("from post", this.state.post);
  }
  render() {
    return (
      <div className="post">
        <h4>{this.state.post.title}</h4>
        <p>{this.state.post.body}</p>
      </div>
    );
  }
}
